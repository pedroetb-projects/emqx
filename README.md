# emqx

Deployment of EMQ X, an Open-Source, Cloud-Native, Massive-Scalable MQTT Broker for IoT

## Configuration

You should set some configuration values using environment variables. All variables used at `compose.yaml` file (with default values set inline or at `.env` file) are ready to be overwritten.

At least, customize these ones (`DD_` prefix is required by <https://gitlab.com/redmic-project/docker/docker-deploy> to make them available at deployment):

* `DD_IMAGE_TAG`: Docker image tag to pull for EMQX service. Default is `latest`.
* `DD_REPLICAS`: Number of replicas for EMQX service. Default is `2`.
* `DD_EMQX_CLUSTER__STATIC__SEEDS`: Static list of replica names, to allow them find each other. Default is `[emqx@emqx1.change.me,emqx@emqx2.change.me]`.
* `DD_EMQX_NODE__COOKIE`: Unique identifier for broker cluster. Default is `emqx-change-me`.
* `DD_TRAEFIK_DOMAIN`: Your public domain name. Default is `change.me`.
* `DD_VOL_ADDR`: IP address of NFS server, which provides persistent storage. Default is `127.0.0.1`.

## Usage

After deploying services successfully (including external *Traefik* proxy), you will be able to use:

* **EMQX API/Dashboard**, through HTTPS at <https://emqx.change.me>.
* **MQTT over TCP**, through MQTT with default port (`1883`) at <mqtt.change.me>.
* **MQTTWS (over websockets)**, through HTTPS at <https://mqtt.change.me>.

> Note that subdomains and domain are customizable at `TRAEFIK_API_SUBDOMAIN`, `TRAEFIK_MQTT_SUBDOMAIN` and `TRAEFIK_DOMAIN` environment variables.

API/Dashboard is usable from any web browser and MQTT can be tested using [Mosquitto client](https://mosquitto.org) from CLI:

```sh
# subscribe
mosquitto_sub -h mqtt.change.me -u testuser -P testpass -t testtopic

# publish
mosquitto_pub -h mqtt.change.me -u testuser -P testpass -t testtopic -m "testmessage"
```

By default, this connection will fail, because authorization is not configured yet.

## Auth management

Check official docs about this topic here:

* <https://www.emqx.io/docs/en/v5.5/access-control/authn/mnesia.html#use-built-in-database>
* <https://www.emqx.io/docs/en/v5.5/access-control/authz/file.html#use-acl-file>

Built-in database (based on *Mnesia*) is used. You can populate and edit auth from API/Dashboard and any change is replicated to all nodes in cluster.

Be careful with `EMQX_AUTHORIZATION__NO_MATCH` variable. If you set it to `allow` (`deny` by default), access will be granted when no rules are found to authorize connection.
